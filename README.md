# BrewerMicro
Microscopio para monitoreo de levaduras cerveceras

<img src="/Imagenes/20190320_171323.jpg" width="400"/> <img src="/Imagenes/vlcsnap-2019-03-22-20h01m24s733.png" width="400"/>

El proyecto tiene como objetivo desarrollar un microscopio abierto de bajo costo para conteo de levaduras cerveceras.
Este diseño es un híbrido basado en la combinación de otros microscopios abiertos.
- [Flypi](https://github.com/amchagas/Flypi)
- [DIY microscope](https://hackteria.org/wiki/DIY_microscopy)
- [Openflexure](https://github.com/rwb27/openflexure_microscope)
- [Fluopi](https://osf.io/dy6p2/) 

Durante el Taller de Ciencia y Cerveza dictado por el [IPATEC](https://ipatec.conicet.gov.ar/ciencia-cerveza/) en Marzo 2019 en Valdivia hicimos pruebas con varios [prototipos](/Prototipos.md).

>>>
Este proyecto es una colaboración entre:
- [Fernan Federici](https://federicilab.org/about/) 
- [Gudrun Kausel](http://www.ciencias.uach.cl/instituto/bioquimica_microbiologia/investigacion/gudrun_kausel.php)
- [Fernando Castro](https://gitlab.com/nanocastro/)
- [Carlos Bertoli](https://ipatec.conicet.gov.ar/ciencia-cerveza/)
>>>




