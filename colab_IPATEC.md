De lo conversado con Carlos (12/4/19)

Desde IPATEC creen que estaría bueno armar un Kit de dispositivos para cerveceros. 
Ese kit podría incluir: 
- microscopio (para recuento/tasa de inoculación)
- densimetro/termometro on-line. uno de los requisitos sería que resista la limpieza (bocha cif)
- colorímetro. para determinar turbidez y color como parametros de calidad de la cerveza)

La idea es comenzar con el microscopio. 

Lo ideal sería que se pudiera usar el telefono pero... 
En caso de la usar camaras externas la propuesta es tratar de disminuir los movimientos, es decir, no usar manipulador y tampoco usar camara Neubauer. 
Aquí hay que terminar de definir la forma pero la idea sería simplificar.   
Carlos cree que se puede armar una slide de volumen conocido y a partir de conocer las caracterìsticas de la imagen ellos podrìan investigar un mètodo para hacer el conteo. 

Para esto nosotros (Fernan y Nano) tendrìamos que poder [definir esas caracterìsticas](https://github.com/rwb27/usaf_analysis), es decir, field of view/resolución de cada setup lente/camara/iluminación/imagen que queramos probar.

La idea es que una vez hecho esto ellos puedan replicar el/los prototipos y testear métodos de recuento.

En cuanto a la forma de colaborar quedamos en trabajar a la distancia sobre este respositorio y ver la posibilidad de encontrarnos a trabajar en un futuro cercano ya sea por Bariloche, Mendoza o Valdivia.



