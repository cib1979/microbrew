# Prototipos

Todos los prototipos se basan en [hackear una cámara web](https://hackteria.org/wiki/DIY_microscopy) dando vuelta el objetivo para magnificar la imagen.  
Hicimos pruebas observando levaduras en cámara Neubauer.  
El manipulador y la base están diseñados en OpenScad e impresos en 3D.  
Para tener una mejor idea de lo que implica mirar levaduras al microscopio se puede consultar el [material educativo del IPATEC](https://ipatec.conicet.gov.ar/presentaciones/)   

## Prototipo 1
Basado en OpenFlexure con cámara weby objetivo de 40X.   
Eje Z basado en .....   
Luz led en brazo articulado.  

<img src="/Imagenes/20190323_083351.jpg" width="400"/> <img src="/Imagenes/20190125_211040.jpg" width="400"/>

## Prototipo 2 
Cámara web China. Es la misma utilizada en el [Community microscope de Public Lab](https://store.publiclab.org/collections/featured-kits/products/microscope?variant=8179759612011)  
Manipulador de Flypi modificado.   
Luz led en brazo articulado.  

<img src="/Imagenes/20190320_171323.jpg" width="400"/> <img src="/Imagenes/vlcsnap-2019-03-22-19h51m02s258.png" width="400"/>

## Prototipo 3
Cámara web Logitech C270 con lente M12 de distancia focal 2.5mm.   
Manipulador Flypi mmodificado.   
Luz led en brazo articulado.  

<img src="/Imagenes/IMG_4110.JPG" width="400"/> <img src="/Imagenes/vlcsnap-2019-03-22-20h49m53s581.png" width="400"/>

# Conclusiones/Issues
- Documentación
    - falta BOM de cada prototipo
    - agregar diseños .scad faltantes
- Iluminación: 
    - ajustar la impresión a los tornillos disponibles (M4 por defecto pero dificil de conseguir en Arg y Chile). Adaptar a 5/32" o 1/8"
    - necesitamos hacer que la luz quede alineada con el objetivo
    - agregar condensador para ayudar a una mejor resolución
    - agregar amarre a la base
- Manipulador
    - mejorar diseño para eliminar vibraciones
    - doble tuerca para eje Z para que no se incline
    - amarre de la base a la superficie donde se apoya (ie sopapas?)
- Óptica/Detector
    - probar lentes M12 con distinta distancia focal
    - [caracterizar distintos prototipos](https://github.com/rwb27/usaf_analysis) utilizando imagen USAF 
- Experimentación
    - probar con levaduras teñidas con azul de metileno (ie para conteo de levaduras vivas/muertas)

    